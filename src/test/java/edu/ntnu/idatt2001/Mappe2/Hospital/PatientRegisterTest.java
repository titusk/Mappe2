package edu.ntnu.idatt2001.Mappe2.Hospital;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {

    @Test
    @DisplayName("Throws exception when patient did not get removed.")
    public void throwsWhenPatientRemovedUnsuccessfully(){
        ArrayList<Patient> testArrayList = new ArrayList<>();
        PatientRegister testPR = new PatientRegister(testArrayList);
        Patient testPatient = new Patient("A", "B", 12345678901L);
        assertThrows(IllegalArgumentException.class, ()-> testPR.removePatient(testPatient));
    }

    @Test
    @DisplayName("Does not throw exception when a patient is successfully removed.")
    public void doesNotThrowWhenPatientRemovedSuccess(){
        ArrayList<Patient> testArrayList = new ArrayList<>();
        PatientRegister testPR = new PatientRegister(testArrayList);
        Patient testPatient = new Patient("A", "B", 12345678901L);
        testPR.getPatientRegister().add(testPatient);
        assertDoesNotThrow(()-> testPR.removePatient(testPatient));
    }

    @Test
    @DisplayName("Throws exception when Patient's SSN includes non digit symbols")
    public void throwsWhenPatientsSSNIncludesNonDigitSymbols(){
        ArrayList<Patient> testArrayList = new ArrayList<>();
        PatientRegister testPR = new PatientRegister(testArrayList);
        assertThrows(IllegalArgumentException.class, ()-> testPR.addPatient("A", "B", "1234567890z", "", ""));
    }

    @Test
    @DisplayName("Does not throw exception when Patient's SSN includes non digit symbols")
    public void doesNotThrowWhenPatientsSSNDoesNotIncludeNonDigitSymbols(){
        ArrayList<Patient> testArrayList = new ArrayList<>();
        PatientRegister testPR = new PatientRegister(testArrayList);
        assertDoesNotThrow(()-> testPR.addPatient("A", "B", "12345678901L", "", ""));
    }
}

