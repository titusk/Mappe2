package edu.ntnu.idatt2001.Mappe2.Hospital;

import java.util.ArrayList;

/**
 * PatientRegister Class
 *
 * The patient register, patient data storage
 *
 * @author Titus Kristiansen
 * @version v0.9
 */
public class PatientRegister {

    /**
     * Variable creation
     */
    private ArrayList<Patient> patientRegister;

    /**
     * Constructor
     * @param patientRegister
     */
    public PatientRegister(ArrayList<Patient> patientRegister) {
        this.patientRegister = patientRegister;
    }

    /**
     * getPatientRegister Method
     * @return the ArrayList of the register
     */
    public ArrayList<Patient> getPatientRegister() {
        return patientRegister;
    }

    /**
     * removePatient Method
     * @param patient which has to be deleted
     * @throws IllegalArgumentException if the patient does not exist in the list
     */
    public void removePatient(Patient patient) throws IllegalArgumentException{
        if(this.patientRegister.remove(patient)){}
        else{throw new IllegalArgumentException("Object not in list or could not be removed.");}
    }

    /**
     * addPatient Method
     *
     * Checks that the inserted data is valid and adds the patient into the register
     *
     * @param firstName of the patient that is to be added
     * @param lastName of the patient that is to be added
     * @param socialSecurityNumberString of the patient that is to be added
     * @param diagnosis of the patient that is to be added
     * @param generalPractitioner of the patient that is to be added
     */
    public void addPatient(String firstName, String lastName, String socialSecurityNumberString, String diagnosis,
                           String generalPractitioner){

        try{
            if(patientRegister.contains(new Patient(firstName, lastName, Long.parseLong(socialSecurityNumberString),
                    diagnosis, generalPractitioner))){
                System.out.println("Patient already exists!");
            }
            else {
                patientRegister.add(new Patient(firstName, lastName, Long.parseLong(socialSecurityNumberString), diagnosis, generalPractitioner));
            }
        }
        catch (Exception e){
            System.out.println("Social security number has to be 11 digits!");
        }
    }

}