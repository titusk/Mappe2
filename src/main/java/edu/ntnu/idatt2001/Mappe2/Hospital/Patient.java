package edu.ntnu.idatt2001.Mappe2.Hospital;

/**
 * Patient Class
 *
 * Patient Object creation and manipulation
 *
 * @author Titus Kristiansen
 * @version v0.9
 */
public class Patient {

    /**
     * Variable creation
     */
    private String firstName;
    private String lastName;
    private long socialSecurityNumber;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Three parameter constructor
     * @param firstName of the patient
     * @param lastName of the patient
     * @param socialSecurityNumber of the patient
     */
    public Patient(String firstName, String lastName, long socialSecurityNumber) {

        if (firstName == null || firstName.isEmpty() || lastName == null || lastName.isEmpty()){
            throw new IllegalArgumentException("First name/last name fields cannot be empty");
        }else {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        if (String.valueOf(socialSecurityNumber).length() != 11){
            throw new IllegalArgumentException("The social security number has to be 11 digits long");
        }else {
            this.socialSecurityNumber = socialSecurityNumber;
        }

        this.diagnosis = "";
        this.generalPractitioner = "";
    }

    /**
     * Five parameter constructor
     * @param firstName of the patient
     * @param lastName of the patient
     * @param socialSecurityNumber of the patient
     * @param diagnosis of the patient
     * @param generalPractitioner of the patient
     */
    public Patient(String firstName, String lastName, long socialSecurityNumber, String diagnosis,
                   String generalPractitioner) {

        if (firstName == null || firstName.isEmpty() || lastName == null || lastName.isEmpty()){
            throw new IllegalArgumentException("First name/last name fields cannot be empty");
        }else {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        if (String.valueOf(socialSecurityNumber).length() != 11){
            throw new IllegalArgumentException("The social security number has to be 11 digits long");
        }else {
            this.socialSecurityNumber = socialSecurityNumber;
        }

        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Getter Methods
     */
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Setter Methods
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSocialSecurityNumber(long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }
}
