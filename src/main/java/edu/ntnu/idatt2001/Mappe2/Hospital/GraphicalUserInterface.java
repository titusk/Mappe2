package edu.ntnu.idatt2001.Mappe2.Hospital;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.text.Text;

import java.io.File;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;


import java.util.ArrayList;


/**
 * GraphicalUserInterface Class
 *
 * Loads and runs all the graphical elements and their functionality
 *
 * @author Titus Kristiansen
 * @version v0.9
 */
public class GraphicalUserInterface extends Application implements EventHandler<ActionEvent> {

    /**
     * Variable creation
     *
     * Variables created here are used among the methods, the rest are local variables
     */
    private PatientRegister patientRegister;
    private Patient activePatient;

    private Button addPatientButton, removePatientButton, editPatientButton, addButton, saveButton, cancelButton,
                removeButton;
    private MenuItem importCSVMenuItem, exportCSVMenuItem, exitMenuItem, addPatientMenuItem,
                editPatientMenuItem, removePatientMenuItem, aboutMenuItem;
    private TextField firstNameTextField, lastNameTextField, socialSecurityNumberTextField, diagnosisTextField,
                generalPractitionerTextField;
    private AnchorPane secondaryWindowAnchorPane, tertiaryWindowAnchorPane;
    private Stage secondStage, thirdStage;
    private Scene mainWindowScene, secondScene, thirdScene;
    private TableView<Patient> patientTableView;

    /**
     * start Method
     *
     * Initializes and runs/shows graphical user interface
     * @param stage
     */
    @Override
    public void start(Stage stage){

        initializer();

        try {
            stage.setTitle("Patient Register");
            stage.setScene(mainWindowScene);
            stage.show();
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * initializer Method
     *
     * Initialises the patientRegister and loads the mainWindow
     */
    private void initializer(){

        ArrayList<Patient> patientList = new ArrayList<>();
        patientRegister = new PatientRegister(patientList);
        mainWindow();
    }

    /**
     * mainWindow Method
     *
     * Displays the window(Stage) of the main program
     */
    private void mainWindow(){

        addPatientButton = new Button();
        addPatientButton.setLayoutX(5);
        addPatientButton.setLayoutY(30);
        ImageView addPatientImage = new ImageView(new Image("file:src/main/resources/add-user-icon.png"));
        addPatientImage.setFitHeight(60);
        addPatientImage.setFitWidth(60);
        addPatientButton.setGraphic(addPatientImage);

        editPatientButton = new Button();
        editPatientButton.setLayoutY(30);
        editPatientButton.setLayoutX(86);
        ImageView editPatientImage = new ImageView(new Image("file:src/main/resources/edit-user-icon.png"));
        editPatientImage.setFitHeight(60);
        editPatientImage.setFitWidth(60);
        editPatientButton.setGraphic(editPatientImage);

        removePatientButton = new Button();
        removePatientButton.setLayoutY(30);
        removePatientButton.setLayoutX(167);
        ImageView removePatientImage = new ImageView(new Image("file:src/main/resources/remove-user-icon.png"));
        removePatientImage.setFitHeight(60);
        removePatientImage.setFitWidth(60);
        removePatientButton.setGraphic(removePatientImage);

        importCSVMenuItem = new MenuItem("Import from .CSV ...");
        exportCSVMenuItem = new MenuItem("Export to .CSV ...");
        editPatientMenuItem = new MenuItem("Edit Selected Patient");
        removePatientMenuItem = new MenuItem("Remove Selected Patient");
        aboutMenuItem = new MenuItem("About");
        exitMenuItem = new MenuItem("Exit");
        addPatientMenuItem = new MenuItem("Add new Patient ...");


        Menu fileMenu = new Menu("File");
        fileMenu.getItems().addAll(importCSVMenuItem, exportCSVMenuItem, exitMenuItem);
        Menu editMenu = new Menu("Edit");
        editMenu.getItems().addAll(addPatientMenuItem, editPatientMenuItem,removePatientMenuItem);
        Menu helpMenu = new Menu("Help");
        helpMenu.getItems().addAll(aboutMenuItem);
        MenuBar menuBar = new MenuBar(fileMenu, editMenu, helpMenu);
        menuBar.setPrefWidth(800);

        addPatientButton.setOnAction(this);
        editPatientButton.setOnAction(this);
        removePatientButton.setOnAction(this);
        addPatientMenuItem.setOnAction(this);
        editPatientMenuItem.setOnAction(this);
        removePatientMenuItem.setOnAction(this);
        aboutMenuItem.setOnAction(this);
        exitMenuItem.setOnAction(this);
        importCSVMenuItem.setOnAction(this);
        exportCSVMenuItem.setOnAction(this);


        patientTableView = new TableView<>();
        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstNameColumn.setPrefWidth(150);
        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        lastNameColumn.setPrefWidth(150);
        TableColumn<Patient, String> socialSecurityNumberColumn = new TableColumn<>("SSN");
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        socialSecurityNumberColumn.setPrefWidth(100);
        TableColumn<Patient, String> diagnosisColumn = new TableColumn<>("Diagnosis");
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        diagnosisColumn.setPrefWidth(200);
        TableColumn<Patient, String> generalPractitionerColumn = new TableColumn<>("General practitioner");
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        generalPractitionerColumn.setPrefWidth(198);
        patientTableView.getColumns().addAll(firstNameColumn, lastNameColumn, socialSecurityNumberColumn,
                diagnosisColumn, generalPractitionerColumn);
        patientTableView.setPrefWidth(800);


        ScrollPane tableViewScrollPane = new ScrollPane();
        tableViewScrollPane.setContent(patientTableView);
        tableViewScrollPane.setLayoutY(103);
        tableViewScrollPane.setPrefHeight(440);

        AnchorPane mainWindowAnchorPane = new AnchorPane();
        mainWindowAnchorPane.getChildren().addAll(menuBar, addPatientButton, editPatientButton, removePatientButton, tableViewScrollPane);
        mainWindowScene = new Scene(mainWindowAnchorPane, 800, 500);
    }

    /**
     * addPatientWindow Method
     *
     * Displays the window(Stage) for adding a patient
     */
    private void addPatientWindow(){

        Text firstNameText = new Text("First name:");
        firstNameText.setLayoutX(14);
        firstNameText.setLayoutY(51);

        firstNameTextField = new TextField();
        firstNameTextField.setPrefWidth(200);
        firstNameTextField.setLayoutX(156);
        firstNameTextField.setLayoutY(34);

        Text lastNameText = new Text("Last name:");
        lastNameText.setLayoutX(14);
        lastNameText.setLayoutY(94);

        lastNameTextField = new TextField();
        lastNameTextField.setPrefWidth(200);
        lastNameTextField.setLayoutX(156);
        lastNameTextField.setLayoutY(77);

        Text socialSecurityNumberText = new Text("Social security number:");
        socialSecurityNumberText.setLayoutX(14);
        socialSecurityNumberText.setLayoutY(135);

        socialSecurityNumberTextField = new TextField();
        socialSecurityNumberTextField.setPrefWidth(100);
        socialSecurityNumberTextField.setLayoutX(156);
        socialSecurityNumberTextField.setLayoutY(118);

        Text diagnosisText = new Text("Diagnosis:");
        diagnosisText.setLayoutX(14);
        diagnosisText.setLayoutY(206);

        diagnosisTextField = new TextField();
        diagnosisTextField.setPrefWidth(200);
        diagnosisTextField.setLayoutX(156);
        diagnosisTextField.setLayoutY(189);

        Text generalPractitionerText = new Text("General practitioner:");
        generalPractitionerText.setLayoutX(14);
        generalPractitionerText.setLayoutY(247);

        generalPractitionerTextField = new TextField();
        generalPractitionerTextField.setPrefWidth(200);
        generalPractitionerTextField.setLayoutX(156);
        generalPractitionerTextField.setLayoutY(230);

        addButton = new Button("Add");
        addButton.setLayoutX(300);
        addButton.setLayoutY(280);
        addButton.setPrefHeight(25);
        addButton.setPrefWidth(80);
        addButton.setOnAction(this);

        cancelButton = new Button("Cancel");
        cancelButton.setLayoutX(390);
        cancelButton.setLayoutY(280);
        cancelButton.setPrefWidth(80);
        cancelButton.setPrefHeight(25);
        cancelButton.setOnAction(this);

        secondaryWindowAnchorPane = new AnchorPane();
        secondaryWindowAnchorPane.getChildren().addAll(firstNameText, lastNameText, socialSecurityNumberText,
                diagnosisText, generalPractitionerText, firstNameTextField, lastNameTextField,
                socialSecurityNumberTextField, diagnosisTextField, generalPractitionerTextField, addButton,
                cancelButton);
        secondScene = new Scene(secondaryWindowAnchorPane, 485, 320);

        secondStage = new Stage();
        secondStage.setTitle("Patient Details - Add");
        secondStage.setResizable(false);
        secondStage.setAlwaysOnTop(true);
        secondStage.setScene(secondScene);
        secondStage.show();
    }

    /**
     * editPatientWindow Method
     *
     * Displays the window(Stage) for editing patient data
     */
    private void editPatientWindow(){

        Text firstNameText = new Text("First name:");
        firstNameText.setLayoutX(14);
        firstNameText.setLayoutY(51);

        firstNameTextField = new TextField();
        firstNameTextField.setPrefWidth(200);
        firstNameTextField.setLayoutX(156);
        firstNameTextField.setLayoutY(34);

        Text lastNameText = new Text("Last name:");
        lastNameText.setLayoutX(14);
        lastNameText.setLayoutY(94);

        lastNameTextField = new TextField();
        lastNameTextField.setPrefWidth(200);
        lastNameTextField.setLayoutX(156);
        lastNameTextField.setLayoutY(77);

        Text socialSecurityNumberText = new Text("Social security number:");
        socialSecurityNumberText.setLayoutX(14);
        socialSecurityNumberText.setLayoutY(135);

        socialSecurityNumberTextField = new TextField();
        socialSecurityNumberTextField.setPrefWidth(100);
        socialSecurityNumberTextField.setLayoutX(156);
        socialSecurityNumberTextField.setLayoutY(118);

        Text diagnosisText = new Text("Diagnosis:");
        diagnosisText.setLayoutX(14);
        diagnosisText.setLayoutY(206);

        diagnosisTextField = new TextField();
        diagnosisTextField.setPrefWidth(200);
        diagnosisTextField.setLayoutX(156);
        diagnosisTextField.setLayoutY(189);

        Text generalPractitionerText = new Text("General practitioner:");
        generalPractitionerText.setLayoutX(14);
        generalPractitionerText.setLayoutY(247);

        generalPractitionerTextField = new TextField();
        generalPractitionerTextField.setPrefWidth(200);
        generalPractitionerTextField.setLayoutX(156);
        generalPractitionerTextField.setLayoutY(230);

        saveButton = new Button("Save");
        saveButton.setLayoutX(300);
        saveButton.setLayoutY(280);
        saveButton.setPrefHeight(25);
        saveButton.setPrefWidth(80);
        saveButton.setOnAction(this);

        cancelButton = new Button("Cancel");
        cancelButton.setLayoutX(390);
        cancelButton.setLayoutY(280);
        cancelButton.setPrefWidth(80);
        cancelButton.setPrefHeight(25);
        cancelButton.setOnAction(this);

        secondaryWindowAnchorPane = new AnchorPane();
        secondaryWindowAnchorPane.getChildren().addAll(firstNameText, lastNameText, socialSecurityNumberText,
                diagnosisText, generalPractitionerText, firstNameTextField, lastNameTextField,
                socialSecurityNumberTextField, diagnosisTextField, generalPractitionerTextField, saveButton, cancelButton);
        secondScene = new Scene(secondaryWindowAnchorPane, 485, 320);

        secondStage = new Stage();
        secondStage.setTitle("Patient Details - Edit");
        secondStage.setResizable(false);
        secondStage.setAlwaysOnTop(true);
        secondStage.setScene(secondScene);
        secondStage.show();
    }

    /**
     * removePatientWindow Method
     *
     * Displays the confirmation dialog for patient deletion
     */
    private void removePatientWindow(){

        Text warningText1 = new Text("Are you sure you want to delete this patient?");
        warningText1.setStyle("-fx-font: 18 arial;");
        warningText1.setLayoutX(60);
        warningText1.setLayoutY(75);

        Text warningText2 = new Text("You can't undo this action.");
        warningText2.setStyle("-fx-font: 18 arial;");
        warningText2.setLayoutX(130);
        warningText2.setLayoutY(105);

        removeButton = new Button("Delete");
        removeButton.setLayoutX(300);
        removeButton.setLayoutY(170);
        removeButton.setPrefHeight(25);
        removeButton.setPrefWidth(80);
        removeButton.setOnAction(this);

        cancelButton = new Button("Cancel");
        cancelButton.setLayoutX(390);
        cancelButton.setLayoutY(170);
        cancelButton.setPrefWidth(80);
        cancelButton.setPrefHeight(25);
        cancelButton.setOnAction(this);

        secondaryWindowAnchorPane = new AnchorPane();
        secondaryWindowAnchorPane.getChildren().addAll(warningText1, warningText2, removeButton, cancelButton);
        secondScene = new Scene(secondaryWindowAnchorPane, 485, 210);

        secondStage = new Stage();
        secondStage.setTitle("Patient Details - Remove");
        secondStage.setResizable(false);
        secondStage.setAlwaysOnTop(true);
        secondStage.setScene(secondScene);
        secondStage.show();
    }

    /**
     * aboutWindow Method
     *
     * Displays the "About" window
     */
    private void aboutWindow(){
        secondaryWindowAnchorPane = new AnchorPane();

        Text versionText = new Text("Patient Register\nv1.0");
        versionText.setLayoutX(10);
        versionText.setLayoutY(30);
        Text authorText = new Text("Application created by \nTitus Kristiansen\n2021-05-04");
        authorText.setLayoutX(10);
        authorText.setLayoutY(90);

        cancelButton = new Button("OK");
        cancelButton.setLayoutX(200);
        cancelButton.setLayoutY(120);
        cancelButton.setPrefWidth(80);
        cancelButton.setPrefHeight(25);
        cancelButton.setOnAction(this);

        secondaryWindowAnchorPane.getChildren().addAll(versionText, authorText, cancelButton);
        secondScene = new Scene(secondaryWindowAnchorPane, 300, 165);

        secondStage = new Stage();
        secondStage.setTitle("Information Dialog - About");
        secondStage.setResizable(false);
        secondStage.setAlwaysOnTop(true);
        secondStage.setScene(secondScene);
        secondStage.show();
    }



    /**
     * missingNameWindow Method
     *
     * Displays a window that tells the user that the data entered is invalid
     */
    private void missingNameWindow(){
        tertiaryWindowAnchorPane = new AnchorPane();

        Text patientText = new Text("The patient must have a name and a valid SSN!");
        patientText.setStyle("-fx-font: 12 arial");
        patientText.setLayoutX(20);
        patientText.setLayoutY(75);
        tertiaryWindowAnchorPane.getChildren().addAll(patientText);
        thirdScene = new Scene(tertiaryWindowAnchorPane, 300, 150);

        thirdStage = new Stage();
        thirdStage.setTitle("Dialog - Wrong Information");
        thirdStage.setResizable(false);
        thirdStage.setAlwaysOnTop(true);
        thirdStage.setScene(thirdScene);
        thirdStage.show();
    }

    /**
     * missingSelectionWindow Method
     *
     * Displays a prompt to tell the user that they have to choose a task from the list
     */
    private void missingSelectionWindow(){
        tertiaryWindowAnchorPane = new AnchorPane();

        Text missingPatientText = new Text("Choose a patient from the list first!");

        missingPatientText.setStyle("-fx-font: 17 arial");
        missingPatientText.setLayoutX(20);
        missingPatientText.setLayoutY(75);

        tertiaryWindowAnchorPane.getChildren().addAll(missingPatientText);
        thirdScene = new Scene(tertiaryWindowAnchorPane, 300, 150);

        thirdStage = new Stage();
        thirdStage.setTitle("Dialog - Wrong Information");
        thirdStage.setResizable(false);
        thirdStage.setAlwaysOnTop(true);
        thirdStage.setScene(thirdScene);
        thirdStage.show();
    }

    /**
     * importCSV Method
     *
     * Imports the CSV file and displays the patient
     */
    private void importCSV() throws IOException{
        FileChooser csvChooser = new FileChooser();
        csvChooser.setTitle("Import .CSV");
        csvChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All files","*.csv"));
        File selectedFile = csvChooser.showOpenDialog(secondStage);
        FileReader fileReader = new FileReader(selectedFile);
        CSVReader csvReader = new CSVReader(fileReader);

        String[] nextLine;
        while ((nextLine = csvReader.readNext()) != null){
            for (var e: nextLine){
                String[] patient = e.split(";");
                try{
                    patientRegister.addPatient(patient[0], patient[1], patient[3], "", patient[2]);
                }
                catch (IllegalArgumentException i){
                }
            }
        }



    }

    /**
     * exportCSV Method
     *
     * No functionality
     */
    private void exportCSV(){
        FileChooser csvChooser = new FileChooser();
        csvChooser.setTitle("Export .CSV");
        csvChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All files","*.csv"));
        String selectedPath = csvChooser.showOpenDialog(secondStage).getPath();
    }

    /**
     * refresh Method
     *
     * Refreshes the patientTableView to display the current data
     */
    private void refreshPatientTableView(){
        patientTableView.getItems().clear();
        for(Patient patient: patientRegister.getPatientRegister()){
            patientTableView.getItems().add(patient);
        }
    }

    /**
     * backToMainScene Method
     *
     * Closes the pop up window
     */
    private void backToMainScene(){
        secondStage.close();
    }

    /**
     * handle Method
     *
     * Handles the ActionEvents of all Button and Menu Item clicks
     * @param actionEvent
     */
    @Override
    public void handle(ActionEvent actionEvent){

        if(actionEvent.getSource()==addPatientButton || actionEvent.getSource()==addPatientMenuItem){
            addPatientWindow();
        }

        if(actionEvent.getSource()==editPatientButton || actionEvent.getSource()==editPatientMenuItem){
            if(patientTableView.getSelectionModel().getSelectedItem() == null){
                missingSelectionWindow();
            }
            else {
                activePatient = patientTableView.getSelectionModel().getSelectedItem();

                editPatientWindow();
                firstNameTextField.setText(activePatient.getFirstName());
                lastNameTextField.setText(activePatient.getLastName());
                socialSecurityNumberTextField.setText(String.valueOf(activePatient.getSocialSecurityNumber()));
                diagnosisTextField.setText(activePatient.getDiagnosis());
                generalPractitionerTextField.setText(activePatient.getGeneralPractitioner());}
        }

        if(actionEvent.getSource()==removePatientButton || actionEvent.getSource()==removePatientMenuItem){
            if(patientTableView.getSelectionModel().getSelectedItem() == null){
                missingSelectionWindow();
            }
            else {
                removePatientWindow();
            }
        }

        if(actionEvent.getSource()==importCSVMenuItem){
            try {
                importCSV();
            } catch (IOException e) {
            }
            refreshPatientTableView();
        }

        if(actionEvent.getSource()==exportCSVMenuItem){
            exportCSV();
            refreshPatientTableView();
        }

        if(actionEvent.getSource()==aboutMenuItem){ aboutWindow(); }

        if(actionEvent.getSource()==exitMenuItem){ System.exit(0); }

        if(actionEvent.getSource()==addButton){
            if(firstNameTextField.getText() == null || firstNameTextField.getText().equals("") ||
                    lastNameTextField.getText() == null || lastNameTextField.getText().equals("") ||
                    String.valueOf(socialSecurityNumberTextField.getText()).length() != 11)
            { missingNameWindow(); }

            else {
                patientRegister.addPatient(firstNameTextField.getText(), lastNameTextField.getText(),
                        socialSecurityNumberTextField.getText(), diagnosisTextField.getText(),
                        generalPractitionerTextField.getText());
                refreshPatientTableView();
                backToMainScene();
                secondStage.close();
        }
        }

        if(actionEvent.getSource()==saveButton){
            if(firstNameTextField.getText() == null || firstNameTextField.getText().equals("") ||
                    lastNameTextField.getText() == null || lastNameTextField.getText().equals("") ||
                    String.valueOf(socialSecurityNumberTextField.getText()).length() != 11)
            { missingNameWindow(); }
            else {
                activePatient.setFirstName(firstNameTextField.getText());
                activePatient.setLastName(lastNameTextField.getText());
                activePatient.setSocialSecurityNumber(Long.parseLong(socialSecurityNumberTextField.getText()));
                activePatient.setDiagnosis(diagnosisTextField.getText());
                activePatient.setGeneralPractitioner(generalPractitionerTextField.getText());
                refreshPatientTableView();
                backToMainScene();

            }
        }

        if(actionEvent.getSource()==cancelButton){
            secondStage.close();
        }

        if(actionEvent.getSource()==removeButton){

            patientRegister.removePatient(patientTableView.getSelectionModel().getSelectedItem());
            refreshPatientTableView();
            backToMainScene();
        }
    }
}
