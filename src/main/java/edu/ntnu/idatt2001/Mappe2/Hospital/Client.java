package edu.ntnu.idatt2001.Mappe2.Hospital;

import javafx.application.Application;

public class Client{

    public static void main(String[] args) {
        Application.launch(GraphicalUserInterface.class, args);
    }
}